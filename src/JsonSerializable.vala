namespace Tlg {
    /**
     * Auxiliary interface used to serialize object to TDLib-compatible JSON.
     *
     * Built on top of {@link Json.gobject_serialize}.
     */
    [Version (since = "1.0")]
    public interface JsonSerializable : Object {
        /**
         * Serialize the object.
         *
         * @return {@link Json.Node} which represents the object
         */
        public virtual Json.Node serialize () {
            var node = Json.gobject_serialize (this);
            underscore_object (node.get_object ());
            return node;
        }

        /**
         * Recursively converts JSON object from GLib format to TDLibformat.
         *
         * ### Example:
         * {{{
         *  class MyObject : Object, Tlg.JsonSerializable {
         *      public string some_str  { get; set; }
         *      public bool   some_bool { get; set; }
         *      public int    some_int  { get; set; }
         *
         *      public MyObject (string some_str, bool some_bool, int some_int) {
         *          this.some_str = some_str;
         *          this.some_bool = some_bool;
         *          this.some_int = some_int;
         *      }
         *  }
         *
         *  string json_to_string (Json.Node node) {
         *      var generator = new Json.Generator ();
         *      generator.set_root (node);
         *      return generator.to_data (null);
         *  }
         *
         *  void main () {
         *      var obj = new MyObject ("test", true, 3);
         *      stdout.printf ("%s\n", json_to_string (Json.gobject_serialize (obj)));
         *      // Output:
         *      // {"some-str": "test", "some-bool": true, "some-int": 3}
         *      stdout.printf ("%s\n", json_to_string (obj.serialize ()));
         *      // Output:
         *      // {"some_str": "test", "some_bool": true, "some_int": 3}
         *  }
         * }}}
         */
        private static void underscore_object (Json.Object object) {
            var members = object.get_members ();
            return_if_fail (members != null);

            foreach (var name in members) {
                var node = object.get_member (name);
                return_if_fail (node != null);

                var new_name = name.replace ("-", "_");

                switch (node.get_node_type ()) {
                case Json.NodeType.OBJECT:
                    underscore_object (node.get_object ());
                    break;
                case Json.NodeType.ARRAY:
                    underscore_array (node.get_array ());
                    break;
                }

                object.set_member (new_name, node);

                object.remove_member (name);
            }
        }

        /**
         * Recursively converts JSON array from GLib format to TDLib format.
         *
         * @see Tlg.JsonSerializable.underscore_object
         */
        private static void underscore_array (Json.Array array) {
            array.foreach_element ((arr, i, node) => {
                switch (node.get_node_type ()) {
                case Json.NodeType.OBJECT:
                    underscore_object (node.get_object ());
                    break;
                case Json.NodeType.ARRAY:
                    underscore_array (node.get_array ());
                    break;
                }
            });
        }
    }
}