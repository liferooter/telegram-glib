namespace Tlg {
    /**
     * Parameters of TDLib client.
     */
    [Version (since = "1.0")]
    public class Parameters : Object, JsonSerializable {
        private Client client;
        private bool    _use_test_dc                = false;
        private string  _database_directory         = Path.build_filename (
                                                        Environment.get_user_data_dir (),
                                                        "telegram-glib"
                                                    );
        private string  _files_directory            = null;
        private bool    _use_file_database          = false;
        private bool    _use_chat_info_database     = false;
        private bool    _use_message_database       = false;
        private bool    _use_secret_chats           = false;
        private int     _api_id                     = -1;
        private string  _api_hash                   = null;
        private string  _system_language_code       = Intl.get_language_names ()[0].split (".")[0].replace ("_", "-");
        private string  _device_model               = "telegram-glib";
        private string  _system_version             = null;
        private string  _application_version        = Config.VERSION;
        private bool    _enable_storage_optimizer   = true;
        private bool    _ignore_file_names          = false;

        /**
         * Whether to use test environment.
         *
         * If set to true, the Telegram test environment
         * will be used instead of the production environment.
         */
        [Version (since = "1.0")]
        public bool use_test_dc {
            get {
                return _use_test_dc;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _use_test_dc = value;
            }
        }

        /**
         * The path to the directory for the persistent database.
         */
        [Version (since = "1.0")]
        public string database_directory {
            get {
                return _database_directory;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _database_directory = value;
            }
        }

        /**
         * The path to the directory for storing files.
         *
         * If empty, {@link database_directory} will be used.
         */
        [Version (since = "1.0")]
        public string files_directory {
            get {
                return _files_directory;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _files_directory = value;
            }
        }

        /**
         * Whether to use file database.
         *
         * If set to true, information about downloaded
         * and uploaded files will be saved between application restarts.
         */
        [Version (since = "1.0")]
        public bool use_file_database {
            get {
                return _use_file_database;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _use_file_database = value;
            }
        }

        /**
         * Whether to use chat info database.
         *
         * If set to true, the library will maintain a cache of users,
         * basic groups, supergroups, channels and secret chats.
         * Implies use_file_database.
         */
        [Version (since = "1.0")]
        public bool use_chat_info_database {
            get {
                return _use_chat_info_database;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _use_chat_info_database = value;
                if (value) use_file_database = true;
            }
        }

        /**
         * Whether to use message database.
         *
         * If set to true, the library will maintain a cache of chats and messages.
         * Implies use_chat_info_database.
         */
        [Version (since = "1.0")]
        public bool use_message_database {
            get {
                return _use_message_database;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _use_message_database = value;
                if (value) use_chat_info_database = true;
            }
        }

        /**
         * Whether to use secret chats.
         *
         * If set to true, support for secret chats will be enabled.
         */
        [Version (since = "1.0")]
        public bool use_secret_chats {
            get {
                return _use_secret_chats;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _use_secret_chats = value;
            }
        }

        /**
         * Application identifier for Telegram API access, which can be obtained at [[https://my.telegram.org]].
         */
        [Version (since = "1.0")]
        public int32 api_id {
            get {
                return _api_id;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _api_id = value;
            }
        }

        /**
         * Application identifier hash for Telegram API access, which can be obtained at [[https://my.telegram.org]].
         */
        [Version (since = "1.0")]
        public string api_hash {
            get {
                return _api_hash;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _api_hash = value;
            }
        }

        /**
         * IETF language tag of the user's operating system language.
         *
         * Must be non-empty.
         */
        [Version (since = "1.0")]
        public string system_language_code {
            get {
                return _system_language_code;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _system_language_code = value;
            }
        }

        /**
         * Model of the device the application is being run on.
         *
         * Must be non-empty.
         */
        [Version (since = "1.0")]
        public string device_model {
            get {
                return _device_model;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _device_model = value;
            }
        }

        /**
         * Version of the operating system the application is being run on.
         *
         * If empty, the version is automatically detected by TDLib.
         */
        [Version (since = "1.0")]
        public string system_version {
            get {
                return _system_version;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _system_version = value;
            }
        }

        /**
         * Application version.
         *
         * Must be non-empty.
         */
        [Version (since = "1.0")]
        public string application_version {
            get {
                return _application_version;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _application_version = value;
            }
        }

        /**
         * Whether to enable storage optimizer.
         *
         * If set to true, old files will automatically be deleted.
         */
        [Version (since = "1.0")]
        public bool enable_storage_optimizer {
            get {
                return _enable_storage_optimizer;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _enable_storage_optimizer = value;
            }
        }

        /**
         * Whether to ignore file names.
         *
         * If set to true, original file names will be ignored. Otherwise, downloaded files will be saved under names as close as possible to the original name.
         */
        [Version (since = "1.0")]
        public bool ignore_file_names {
            get {
                return _ignore_file_names;
            }
            set {
                return_if_fail (client == null || !client.is_running);
                _ignore_file_names = value;
            }
        }

        /**
         * Sets TDLib client which this parameters is for.
         *
         * Used to track whether the client is running.
         *
         * @param client Client to track
         */
        internal void set_client (Client client) {
            this.client = client;
        }
    }
}