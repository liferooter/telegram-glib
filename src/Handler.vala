namespace Tlg {

    /**
     * Generic event handler.
     *
     * Used to handle TDLib events.
     */
    [Version (since = "1.0")]
    public interface EventHandler : Object {
        /**
         * Checks whether `event` should be handled.
         *
         * @param event Event to check
         * @return Whether `event` should be handled
         */
        [Version (since = "1.0")]
        public abstract bool match (Json.Object event);
        /**
         * Emmited when matching event is received.
         *
         * Connect callback to this signal to process matching events.
         *
         * @param event Event to handle
         * @return Whether the handler should remain
         */
        [Version (since = "1.0")]
        public abstract signal bool handle (Json.Object event);
    }

    /**
     * Auxilary class used to make asynchronous requests to TDLib.
     */
    [Version (since = "1.0")]
    internal class ResponseHandler : Object, EventHandler {
        private uint64   id;
        private SourceFunc callback;

        public Json.Object event = null;

        public ResponseHandler (SourceFunc callback, uint64 id) {
            this.id = id;
            this.callback = callback;
            handle.connect (handle_event);
        }

        bool handle_event (Json.Object event) {
            lock (this.event) {
                this.event = event;
            }
            Idle.add(callback);
            return false;
        }

        public bool match (Json.Object event) {
            return event.has_member ("@extra") && event.get_int_member ("@extra") == id;
        }
    }
}