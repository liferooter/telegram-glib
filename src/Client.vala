namespace Tlg {
    /**
     * Client class that is used to access API.
     *
     * @since 1.0
     */
    public class Client : Object {

        /* Private properties */
        private void                  *client;
        private Thread<void>           mainloop_thread;
        private Array<EventHandler>    event_handlers;
        private string                 encryption_key = "";
        private Mutex                  process_recieved = Mutex ();
        private uint64                 request_counter = 0;

        /* Public properties */

        /**
         * Whether the client's main loop is running.
         */
        [Version (since = "1.0")]
        public bool is_running { get; private set; default = false; }

        /**
         * Event waiting timeout.
         */
        [Version (since = "1.0")]
        public double wait_time { get; set; default = 10.0; }

        /**
         * Parameters of TDLib.
         */
        [Version (since = "1.0")]
        public Parameters parameters { get; default = new Parameters (); }

        construct {
            client = TdJson.create ();
            event_handlers = new Array<EventHandler> (false, false, sizeof (EventHandler));
            parameters.set_client (this);
        }

        ~Client () {
            TdJson.destroy (client);
        }

        /**
         * Sets TDLib parameters.
         *
         * Must be called before client is running.<<BR>>
         * For more info see [[https://core.telegram.org/tdlib/docs/classtd_1_1td__api_1_1tdlib_parameters.html|TDLib documentation]]
         *
         * @param new_parameters Parameters to set
         * @return Whether new parameters are set
         */
        [Version (since = "1.0")]
        public bool set_parameters (Parameters new_parameters)
            requires (!is_running) {

            new_parameters.set_client (this);
            lock (parameters) {
                _parameters = new_parameters;
            }
            // There is no setter of parameters,
            // so we have to notify manually

            notify_property ("parameters");
            return true;
        }

        /**
         * Sets database encryption key.
         *
         * Should be called before client is running.
         *
         * @param encryption_key Encryption key
         */
        [Version (since = "1.0")]
        public void set_encryption_key (string encryption_key)
            requires (!is_running) {
            lock (this.encryption_key) {
                this.encryption_key = Base64.encode ((uchar[]) encryption_key.data);
            }
        }

        /**
         * Runs client main loop in the external thread.
         *
         * You should set parameters before it's called.
         */
        [Version (since = "1.0")]
        public void run ()
            requires (!is_running)
            requires (parameters.api_hash != null)
            requires (parameters.api_id != -1)
            requires (parameters.system_language_code != null)
            requires (parameters.system_language_code != "")
            requires (parameters.device_model != null)
            requires (parameters.device_model != "")
            requires (parameters.application_version != null)
            requires (parameters.application_version != "")
        {
            if (!Thread.supported ())
                critical ("Cannot run without threads");
            else if (!is_running) {
                is_running = true;
                mainloop_thread = new Thread<void> ("Client thread", mainloop);
            } else {
                warning ("Client is already running");
            }
        }

        /**
         * Add the event handler to the client.
         *
         * @param handler The event handler to add
         * @return The ID of the handler just added
         */
        [Version (since = "1.0")]
        public uint add_event_handler (EventHandler handler) {
            var id = event_handlers.length;
            lock (event_handlers) {
                event_handlers.append_val (handler);
            }
            return id;
        }

        /**
         * Remove the handler from the client.
         *
         * @param id The ID of the handler to delete
         */
        [Version (since = "1.0")]
        public void remove_event_handler (uint id) {
            lock (event_handlers) {
                event_handlers.remove_index (id);
            }
        }

        /**
         * Sends JSON request to TDLib.
         *
         * For more information see [[https://core.telegram.org/tdlib/docs/td__json__client_8h.html]].
         *
         * @param request JSON node of TDLib request
         */
        private void send (Json.Node request) {
            var json = new Json.Generator ();
            json.set_root (request);
            TdJson.send (client, json.to_data (null));
        }

        /**
         * Send request and get response asynchroneously.
         *
         * Used only for internal needs to implement
         * asynchronous API calls.
         *
         * @param request Request to TDLib
         * @return Response received from TDLib
         */
        async Json.Object execute_async (Json.Node request) {
            uint64 new_id;
            lock (request_counter) {
                request_counter++;
                new_id = request_counter;
            }
            var response_handler = new ResponseHandler (execute_async.callback, new_id);
            this.add_event_handler (response_handler);
            send (request);
            yield;
            return response_handler.event;
        }

        /**
         * Recieve JSON request from TDLib.
         */
        private Json.Object? receive () {
            // Lock mutex.
            // It's needed because TDLib clears
            // last received string after next is received.

            var json_parser = new Json.Parser.immutable_new ();

            process_recieved.lock ();

            var json = TdJson.receive (client, wait_time);
            if (json == null) {
                process_recieved.unlock ();
                return null;
            } try {
                json_parser.load_from_data (json);
            } catch (Error e) {
                process_recieved.unlock ();
                warning ("Can't parse event: %s", e.message);
                return null;
            }
            process_recieved.unlock ();
            return json_parser.steal_root ().get_object ();
        }

        /**
         * TDLib client main loop.
         *
         * This function is running in external thread.
         */
        private void mainloop () {
            while (is_running) {
                var event = receive ();
                if (event != null) {
                    handle_event.begin (event);
                }
            }
        }

        /**
         * Handles received event.
         *
         * Used in main loop.
         *
         * @param event Event to handle
         */
        private async void handle_event (Json.Object event) {
            var event_type = event.get_string_member ("@type");

            switch (event_type) {
            case "updateAuthorizationState":
                var state = event.get_object_member ("authorization_state");
                var state_type = state.get_string_member ("@type");
                switch (state_type) {
                case "authorizationStateWaitTdlibParameters":
                    // Send parameters
                    send
                    (
                        new Json.Builder ()
                        .begin_object ()
                        .set_member_name ("@type")
                        .add_string_value ("setTdlibParameters")
                        .set_member_name ("parameters")
                        .add_value (parameters.serialize ())
                        .end_object ()
                        .get_root ()
                    );
                    break;
                case "authorizationStateWaitEncryptionKey":
                    // Send encryption key
                    yield execute_async (
                        new Json.Builder ()
                        .begin_object ()
                        .set_member_name ("@type")
                        .add_string_value ("checkDatabaseEncryptionKey")
                        .set_member_name ("encryption_key")
                        .add_string_value (encryption_key)
                        .end_object ()
                        .get_root ()
                    );
                    break;
                default:
                    stdout.printf ("%s\n", state_type);
                    break;
                }
                break;
            case "error":
                if (event.get_int_member ("code") == 406)
                    break;
                error ("TDLib error: %s", event.get_string_member ("message"));
            }

            for (var i = 0; i < event_handlers.length; i++) {
                var handler = event_handlers.index (i);
                if (handler.match (event)) {
                    if (!handler.handle (event))
                        event_handlers.remove_index (i);
                }
            }
        }

        /**
         * Quit TDLib client main loop.
         *
         * Should be called at the end on program to stop loop
         * and destruct {@link Tlg.Client} instance gracefully.
         */
        public void quit () {
            lock (is_running) {
                is_running = false;
            }
            mainloop_thread.join ();
        }
    }
}
