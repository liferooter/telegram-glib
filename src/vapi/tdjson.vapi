[CCode (cheader_filename = "td/telegram/td_json_client.h", lower_case_cprefix = "td_json_client_")]
namespace TdJson {
  void  *create  ();
  void   destroy (void  *client);

  void            send    (void  *client,
                          string request);
  unowned string? receive (void  *client,
                          double timeout);

  string execute (void  *client,
                  string request);
}
